PDS module for Drupal

** DESCRIPTION **
PDS is a developer API for storing and retrieving data based on Drupal paths.

** INSTALLATION **
After enabling the module set the permission for which role you want PDS item creation to be available for at Administer >> User management >> Permissions.

** USAGE **

Vist Administer >> Site building >> PDS to define PDS items.

PDS items can be retrieved via the PDS API.

Public functions:
pds_load() takes one parameter, the string name collection for your PDS items.
pds_inherit_load() takes one parameter, the string name collection for your PDS items.

** SUPPORT **
Please use the issue queue at http://drupal.org/project/pds

** CREDITS **
Written and maintained by Ben Jeavons (Drupal.org username: coltrane) (benjamin DOT jeavons AT colorado DOT edu). Please use the issue queue for any questions or support.
