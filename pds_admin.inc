<?php

/**
 * @file Administration pages and forms for managing PDS items.
 */

/**
 * Private function to retrieve all PDS data.
 */
function _pds_items_load() {
  $pds_items = array();
  $result = db_query("SELECT * FROM {pds_items}");
  while ($data = db_fetch_array($result)) {
    $pds_items[] = $data;
  }
  return $pds_items;
}

/**
 * Private function loads all names.
 */
function _pds_names_load() {
  $pds_names = array();
  $result = db_query("SELECT DISTINCT(pds_name) FROM {pds_items}");
  while ($data = db_fetch_array($result)) {
    $pds_names[] = $data;
  }
  return $pds_names;
}

/**
 * Menu callback, lists PDS items by name collection.
 */
function pds_overview() {
  $pds_names = _pds_names_load();
  foreach ($pds_names as $name) {
    $items[] = l($name['pds_name'], 'admin/build/pds/' . $name['pds_name']);
  }
  if (empty($items)) {
    drupal_set_message(t('No PDS namespaces or items exist, please create one.'));
    drupal_goto('admin/build/pds/add');
  }
  else {
    $output = theme('item_list', $items, t('Names'));
  }
  
  return $output;
}

/**
 * Menu callback, returns list of PDS data items.
 */ 
function pds_item_list($pds_items = NULL) {
  $name = $pds_items[0]['pds_name'];
  $links[] = l(t('Order @name keys', array('@name' => $name)), 'admin/build/pds/order/' . $name);
  $links[] = l(t('Add a new item'), 'admin/build/pds/add/' . $name);
  $output = theme('item_list', $links);
  $header = array(
    t('Type/Unit'),
    array('data' => t('Key'), 'field' => 'pds_key'),
    array('data' => t('Value'), 'field' => 'pds_value'),
    array('data' => t('Property'), 'field' => 'pds_properties'),
    array('data' => t('Operations'), 'colspan' => 2),
  );
  foreach ($pds_items as $item) {
    $rows[] = array(
      array('data' => check_plain($item['pds_type'] . ' ' . $item['pds_unit'])),
      array('data' => check_plain($item['pds_key'])),
      array('data' => check_plain($item['pds_value'])),
      array('data' => check_plain($item['pds_properties'])),
      array('data' => l('Edit', 'admin/build/pds/edit/' . $item['pds_id'])),
      array('data' => l('Delete', 'admin/build/pds/delete/' . $item['pds_id'])),
    );
  }
  if ($rows) {
    $output .= theme('table', $header, $rows);
  }
  return $output;
}

/**
 * Form for adding or editing a PDS data item.
 */
function pds_item_form(&$form_state, $pds_item = NULL) {
  if (isset($pds_item[0])) {
    // We've received an array of PDS items, hold on to just the name.
    $pds_name = $pds_item[0]['pds_name'];
    $pds_item = array();
  }
  else {
    $pds_name = $pds_item['pds_name'];
  }
  $form['pds_id'] = array(
    '#type' => 'hidden',
    '#value' => empty($pds_item['pds_id']) ? '' : $pds_item['pds_id'],
  );
  $form['pds_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Machine-readable namespace. Defines a collection of PDS items.'),
    '#required' => TRUE,
    '#default_value' => empty($pds_name) ? '' : $pds_name,
  );
  $form['item_path'] = array(
    '#type' => 'fieldset',
    '#description' => t('Type and unit are used to match the Drupal path.'),
  );
  $form['item_path']['pds_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Type'),
    '#description' => t(' Example, if you are defining a PDS item to appear on node/1, the type is "node".'),
    '#required' => TRUE,
    '#default_value' => empty($pds_item['pds_type']) ? '' : $pds_item['pds_type'],
  );
  $form['item_path']['pds_unit'] = array(
    '#type' => 'textfield',
    '#title' => t('Unit'),
    '#description' => t("Example, the unit for a PDS item on a node's page is the node ID."),
    '#required' => TRUE,
    '#default_value' => empty($pds_item['pds_unit']) ? '' : $pds_item['pds_unit'],
  );
  $form['item_value'] = array(
    '#type' => 'fieldset',
    '#description' => t("Define the key-value pair to be stored on this item's path (type and unit)."),
  );
  $form['item_value']['pds_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#description' => t('Case sensitive identifier for this item. For example, "Category" is not the same as "category".'),
    '#required' => TRUE,
    '#default_value' => empty($pds_item['pds_key']) ? '' : $pds_item['pds_key'],
  );
  $form['item_value']['pds_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#description' => t('The value that corresponds to the key for this item.'),
    '#required' => TRUE,
    '#default_value' => empty($pds_item['pds_value']) ? '' : $pds_item['pds_value'],
  );
  $form['properties'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['properties']['pds_properties'] = array(
    '#type' => 'textfield',
    '#title' => t('Property'),
    '#description' => t('Items can have properties that define how they relate to other items in their namespace.'),
    '#required' => FALSE,
    '#default_value' => empty($pds_item['pds_properties']) ? '' : $pds_item['pds_properties'],
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#required' => TRUE,
  );
  $form['cancel'] = array(
    '#type' => 'markup',
    '#value' => l(t('Cancel'), $path),
  );
  return $form;
}

function pds_item_form_submit($form, &$form_state) {
  // Trim the name and the key.
  $form_state['values']['pds_name'] = trim($form_state['values']['pds_name']);
  $form_state['values']['pds_key'] = trim($form_state['values']['pds_key']);
  if (empty($form_state['values']['pds_id'])) {
    // New pds item, unset the PDS element.
    unset($form_state['values']['pds_id']);
    pds_save($form_state['values']);
  }
  else {
    // Update existing item.
    pds_save($form_state['values']);
  }
  // Go to item weight order form.
  $form_state['redirect'] = 'admin/build/pds/order/' . $form_state['values']['pds_name'];
  return;
}

function pds_item_delete_confirm(&$form_state, $pds_item = NULL) {
  if (is_null($pds_item)) {
    drupal_goto('admin/build/pds');
  }
  
  $form['pds_id'] = array('#type' => 'value', '#value' => $pds_item['pds_id']);
  $form['pds_name'] = array('#type' => 'value', '#value' => $pds_item['pds_name']);
  $message = t('Are you sure you want to delete the @name item?', array('@name' => $pds_item['pds_item']));

  return confirm_form($form, $message, 'admin/build/pds', t('This action cannot be undone.'), t('Delete'));
}

function pds_item_delete_confirm_submit($form, &$form_state) {
  pds_delete($form_state['values']);
  $form_state['redirect'] = 'admin/build/pds';
  return;
}

/**
 * Helper function organizes PDS items for setting weights.
 * Duplicate keys are grouped and get the same weight.
 */
function _pds_items_distinct_keys_weights($pds_items) {
  $keys = array();
  foreach ($pds_items as $item) {
    if (!in_array($item['pds_key'], $keys)) {
      $keys[$item['pds_weight']] = $item['pds_key'];
    }
  }
  ksort($keys);
  return $keys;
}

/**
 * Form for ordering pds item keys.
 */
function pds_item_order_form(&$form_state, $pds_items = array()) {
  $form = array();
  $items = _pds_items_distinct_keys_weights($pds_items);
  if (!empty($items)) {
    foreach ($items as $weight => $key) {
      $form[$weight]['pds_key'] = array(
        '#value' => check_plain($key),
      );
      $form[$weight]['pds_weight'] = array(
        '#type' => 'weight',
        '#default_value' => $weight,
      );
    }
    $form['pds_items'] = array(
      '#type' => 'value',
      '#value' => $pds_items,
    );
    $form['pds_name'] = array(
      '#type' => 'value',
      '#value' => $pds_items[0]['pds_name'],
    );
    $form['#redirect'] = 'admin/build/pds/' . $pds_items[0]['pds_name'];
    $form['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
    $form['#tree'] = TRUE;
  }
  return $form;
}

/**
 * Theme the item order form to use tabledrag.
 */
function theme_pds_item_order_form($form) {
  $output = '';
  $header = array(
    t('Item'),
    t('Weight'),
  );
  $rows = array();
  foreach (element_children($form) as $key) {
    if (is_numeric($key)) {
      $element = &$form[$key];
      $row = array();
      $row[] = drupal_render($element['pds_key']);
      $element['pds_weight']['#attributes'] = array('class' => 'pds-order');
      $row[] = drupal_render($element['pds_weight']);
      $rows[] = array('data' => $row, 'class' => 'draggable');
    }
  }
  
  $output = theme('table', $header, $rows, array('id' => 'pds-items-order'));
  
  $output .= drupal_render($form['submit']);
  $output .= drupal_render($form); // Render everything else now.
  
  drupal_add_tabledrag('pds-items-order', 'order', 'sibling', 'pds-order');

  return $output;
}

function pds_item_order_form_submit($form, &$form_state) {
  $pds_items = $form_state['values']['pds_items'];
  $pds_name = $form_state['values']['pds_name'];
  $items = _pds_items_distinct_keys_weights($pds_items);
  foreach ($items as $weight => $key) {
    _pds_item_write_weights($pds_name, $key, $form_state['values'][$weight]['pds_weight']);
  }
  return;
}

/**
 * Since there may be multiple PDS items with the same key we save the weight for each.
 */
function _pds_item_write_weights($pds_name, $pds_key, $pds_weight) {
  $pds_items = _pds_items_key_load($pds_name, $pds_key);
  foreach ($pds_items as $pds_item) {
    $pds_item['pds_weight'] = $pds_weight;
    pds_save($pds_item);
  }
}